<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20201203130742 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE present_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE wish_list_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE wish_list_item_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE present (id INT NOT NULL, name VARCHAR(255) NOT NULL, link VARCHAR(255) DEFAULT NULL, price INT DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE wish_list (id INT NOT NULL, owner_id INT NOT NULL, name VARCHAR(255) NOT NULL, date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_5B8739BD7E3C61F9 ON wish_list (owner_id)');
        $this->addSql('CREATE TABLE wish_list_users (wish_list_id INT NOT NULL, users_id INT NOT NULL, PRIMARY KEY(wish_list_id, users_id))');
        $this->addSql('CREATE INDEX IDX_84B9DCFAD69F3311 ON wish_list_users (wish_list_id)');
        $this->addSql('CREATE INDEX IDX_84B9DCFA67B3B43D ON wish_list_users (users_id)');
        $this->addSql('CREATE TABLE wish_list_item (id INT NOT NULL, present_id INT NOT NULL, wish_list_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9A7FA7118D7B1EF8 ON wish_list_item (present_id)');
        $this->addSql('CREATE INDEX IDX_9A7FA711D69F3311 ON wish_list_item (wish_list_id)');
        $this->addSql('CREATE TABLE wish_list_item_users (wish_list_item_id INT NOT NULL, users_id INT NOT NULL, PRIMARY KEY(wish_list_item_id, users_id))');
        $this->addSql('CREATE INDEX IDX_57EE56215A594A6E ON wish_list_item_users (wish_list_item_id)');
        $this->addSql('CREATE INDEX IDX_57EE562167B3B43D ON wish_list_item_users (users_id)');
        $this->addSql('ALTER TABLE wish_list ADD CONSTRAINT FK_5B8739BD7E3C61F9 FOREIGN KEY (owner_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wish_list_users ADD CONSTRAINT FK_84B9DCFAD69F3311 FOREIGN KEY (wish_list_id) REFERENCES wish_list (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wish_list_users ADD CONSTRAINT FK_84B9DCFA67B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wish_list_item ADD CONSTRAINT FK_9A7FA7118D7B1EF8 FOREIGN KEY (present_id) REFERENCES present (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wish_list_item ADD CONSTRAINT FK_9A7FA711D69F3311 FOREIGN KEY (wish_list_id) REFERENCES wish_list (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wish_list_item_users ADD CONSTRAINT FK_57EE56215A594A6E FOREIGN KEY (wish_list_item_id) REFERENCES wish_list_item (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE wish_list_item_users ADD CONSTRAINT FK_57EE562167B3B43D FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE wish_list_item DROP CONSTRAINT FK_9A7FA7118D7B1EF8');
        $this->addSql('ALTER TABLE wish_list_users DROP CONSTRAINT FK_84B9DCFAD69F3311');
        $this->addSql('ALTER TABLE wish_list_item DROP CONSTRAINT FK_9A7FA711D69F3311');
        $this->addSql('ALTER TABLE wish_list_item_users DROP CONSTRAINT FK_57EE56215A594A6E');
        $this->addSql('DROP SEQUENCE present_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE wish_list_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE wish_list_item_id_seq CASCADE');
        $this->addSql('DROP TABLE present');
        $this->addSql('DROP TABLE wish_list');
        $this->addSql('DROP TABLE wish_list_users');
        $this->addSql('DROP TABLE wish_list_item');
        $this->addSql('DROP TABLE wish_list_item_users');
    }
}
