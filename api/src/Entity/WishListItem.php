<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WishListItemRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=WishListItemRepository::class)
 */
class WishListItem
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class)
     */
    private $Participants;

    /**
     * @ORM\OneToOne(targetEntity=Present::class, inversedBy="WishListItem", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $Present;

    /**
     * @ORM\ManyToOne(targetEntity=WishList::class, inversedBy="wishListItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $WishList;

    public function __construct()
    {
        $this->Participants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return Collection|Users[]
     */
    public function getParticipants(): Collection
    {
        return $this->Participants;
    }

    public function addParticipant(Users $participant): self
    {
        if (!$this->Participants->contains($participant)) {
            $this->Participants[] = $participant;
        }

        return $this;
    }

    public function removeParticipant(Users $participant): self
    {
        if ($this->Participants->contains($participant)) {
            $this->Participants->removeElement($participant);
        }

        return $this;
    }

    public function getPresent(): ?Present
    {
        return $this->Present;
    }

    public function setPresent(Present $Present): self
    {
        $this->Present = $Present;

        return $this;
    }

    public function getWishList(): ?WishList
    {
        return $this->WishList;
    }

    public function setWishList(?WishList $WishList): self
    {
        $this->WishList = $WishList;

        return $this;
    }
}
