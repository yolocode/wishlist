<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\WishListRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource()
 * @ORM\Entity(repositoryClass=WishListRepository::class)
 */
class WishList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToMany(targetEntity=Users::class)
     */
    private $Participants;

    /**
     * @ORM\OneToMany(targetEntity=WishListItem::class, mappedBy="WishList", orphanRemoval=true)
     */
    private $wishListItems;

    /**
     * @ORM\ManyToOne(targetEntity=Users::class, inversedBy="wishLists")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    public function __construct()
    {
        $this->Participants = new ArrayCollection();
        $this->wishListItems = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection|Users[]
     */
    public function getParticipants(): Collection
    {
        return $this->Participants;
    }

    public function addParticipant(Users $participant): self
    {
        if (!$this->Participants->contains($participant)) {
            $this->Participants[] = $participant;
        }

        return $this;
    }

    public function removeParticipant(Users $participant): self
    {
        if ($this->Participants->contains($participant)) {
            $this->Participants->removeElement($participant);
        }

        return $this;
    }

    /**
     * @return Collection|WishListItem[]
     */
    public function getWishListItems(): Collection
    {
        return $this->wishListItems;
    }

    public function addWishListItem(WishListItem $wishListItem): self
    {
        if (!$this->wishListItems->contains($wishListItem)) {
            $this->wishListItems[] = $wishListItem;
            $wishListItem->setWishList($this);
        }

        return $this;
    }

    public function removeWishListItem(WishListItem $wishListItem): self
    {
        if ($this->wishListItems->contains($wishListItem)) {
            $this->wishListItems->removeElement($wishListItem);
            // set the owning side to null (unless already changed)
            if ($wishListItem->getWishList() === $this) {
                $wishListItem->setWishList(null);
            }
        }

        return $this;
    }

    public function getOwner(): ?Users
    {
        return $this->owner;
    }

    public function setOwner(?Users $owner): self
    {
        $this->owner = $owner;

        return $this;
    }
}
